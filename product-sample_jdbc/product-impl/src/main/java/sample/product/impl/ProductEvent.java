package sample.product.impl;

import com.lightbend.lagom.serialization.Jsonable;

import lombok.Value;

public interface ProductEvent extends Jsonable {
    @Value
    public class ProductItemCreated implements ProductEvent {
        private String id;
    }

    @Value
    public class ProductItemRenamed implements ProductEvent {
        private String name;
        private String price;
    }

    @Value
    public class ItemsCheckedInToProduct implements ProductEvent {
        private int count;
    }
}
