package sample.product.impl;

import akka.Done;
import com.lightbend.lagom.javadsl.persistence.PersistentEntity;

import lombok.val;
import sample.product.api.ProductItem;

import sample.product.impl.ProductCommand.*;
import sample.product.impl.ProductEvent.*;

import java.util.Arrays;
import java.util.Optional;

public class ProductEntity extends PersistentEntity<ProductCommand, ProductEvent, ProductItem> {
    @Override
    public Behavior initialBehavior(Optional<ProductItem> snapshotState) {

        val bhvBuilder = newBehaviorBuilder(
                snapshotState.orElse(new ProductItem("", "","", 0)));

        bhvBuilder.setCommandHandler(CreateProductItem.class, (cmd, ctx) -> {
            System.out.println("*** command handle : " + cmd);

            val events = Arrays.asList(
                    new ProductItemCreated(cmd.getId()),
                    new ProductItemRenamed(cmd.getName(), cmd.getPrice())
            );

            return ctx.thenPersistAll(events, () -> ctx.reply(Done.getInstance()));
        });

        bhvBuilder.setCommandHandler(CheckInItemsToProduct.class, (cmd, ctx) -> {
            System.out.println("*** command handle : " + cmd);
            return ctx.thenPersist(new ItemsCheckedInToProduct(cmd.getCount()), ev -> ctx.reply(Done.getInstance()));
        });



        bhvBuilder.setEventHandler(ProductItemCreated.class, ev ->
                new ProductItem(ev.getId(), "","", 0));

        bhvBuilder.setEventHandler(ProductItemRenamed.class, ev ->
                new ProductItem(state().getId(), ev.getName(), ev.getPrice(), state().getCount()));

        bhvBuilder.setEventHandler(ItemsCheckedInToProduct.class, ev ->
                new ProductItem(
                        state().getId(),
                        state().getName(),
                        state().getPrice(),
                        state().getCount() + ev.getCount()));

        bhvBuilder.setReadOnlyCommandHandler(CurrentState.class, (cmd, ctx) -> ctx.reply(state()));

        return bhvBuilder.build();
    }
}
