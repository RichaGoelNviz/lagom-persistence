package sample.product.impl;

import com.lightbend.lagom.javadsl.persistence.PersistentEntity;
import com.lightbend.lagom.serialization.Jsonable;

import akka.Done;
import lombok.Value;
import sample.product.api.ProductItem;

public interface ProductCommand extends Jsonable {
    @Value
    public final class CreateProductItem implements ProductCommand,
            PersistentEntity.ReplyType<Done> {

        private String id;
        private String name;
        private String price;
    }

    @Value
    public final class CheckInItemsToProduct implements ProductCommand,
            PersistentEntity.ReplyType<Done> {

        private int count;
    }

    @Value
    public final class CurrentState implements ProductCommand,
            PersistentEntity.ReplyType<ProductItem> {
    }
}
