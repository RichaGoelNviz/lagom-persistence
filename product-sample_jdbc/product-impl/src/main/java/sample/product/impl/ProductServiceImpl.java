package sample.product.impl;

import akka.Done;
import akka.NotUsed;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRef;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRegistry;

import javax.inject.Inject;

import sample.product.api.ProductCreateParameter;
import sample.product.api.ProductCheckInParameter;
import sample.product.api.ProductItem;
import sample.product.api.ProductService;

import sample.product.impl.ProductCommand.*;

public class ProductServiceImpl implements ProductService {
    private final PersistentEntityRegistry persistentEntityRegistry;

    @Inject
    public ProductServiceImpl(PersistentEntityRegistry persistentEntityRegistry) {

        this.persistentEntityRegistry = persistentEntityRegistry;

        persistentEntityRegistry.register(ProductEntity.class);
    }

    @Override
    public ServiceCall<ProductCreateParameter, Done> create(String id) {
        return req -> getEntityRef(id).ask(new CreateProductItem(id, req.getName(), req.getPrice()));
    }

    @Override
    public ServiceCall<ProductCheckInParameter, Done> checkIn(String id) {
        return req -> getEntityRef(id).ask(new CheckInItemsToProduct(req.getCount()));
    }

    @Override
    public ServiceCall<NotUsed, ProductItem> state(String id) {
        return req -> getEntityRef(id).ask(new CurrentState());
    }

    private PersistentEntityRef<ProductCommand> getEntityRef(String id) {
        return persistentEntityRegistry.refFor(ProductEntity.class, id);
    }
}
