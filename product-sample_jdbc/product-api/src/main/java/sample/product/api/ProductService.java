package sample.product.api;

import static com.lightbend.lagom.javadsl.api.Service.named;
import static com.lightbend.lagom.javadsl.api.Service.pathCall;

import akka.Done;
import akka.NotUsed;
import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.ServiceCall;

public interface ProductService extends Service {

    ServiceCall<ProductCreateParameter, Done> create(String id);

    ServiceCall<ProductCheckInParameter, Done> checkIn(String id);

    ServiceCall<NotUsed, ProductItem> state(String id);

    @Override
    default Descriptor descriptor() {
        return named("product").withCalls(
            pathCall("/product/:id",  this::create),
            pathCall("/product/:id/checkin", this::checkIn),
            pathCall("/product/:id", this::state)
        ).withAutoAcl(true);
    }
}
