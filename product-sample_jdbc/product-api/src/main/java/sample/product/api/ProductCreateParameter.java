package sample.product.api;

import lombok.Value;

@Value
public class ProductCreateParameter {
    private String name;
    private String price;
}
