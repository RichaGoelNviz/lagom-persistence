package sample.product.api;

import lombok.Value;

@Value
public class ProductCheckInParameter {
    private int count;
}
