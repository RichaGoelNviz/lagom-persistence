package sample.product.api;

import com.lightbend.lagom.serialization.Jsonable;

import lombok.Value;

@Value
public final class ProductItem implements Jsonable {
    private final String id;
    private final String name;
    private final String price;
    private final int count;


    public ProductItem(String id, String name, String price, int count) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.count = count;
    }

    @Override
    public boolean equals(Object another) {
        if (this == another)
            return true;
        return another instanceof ProductItem && equalTo((ProductItem) another);
    }

    private boolean equalTo(ProductItem another) {
        return id.equals(another.id) && name.equals(another.name) && price.equals(another.price);
    }

    @Override
    public int hashCode() {
        int h = 31;
        h = h * 17 + id.hashCode();
        h = h * 17 + name.hashCode();
        h = h * 17 + price.hashCode();
        return h;
    }

   

}
